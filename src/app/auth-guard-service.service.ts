import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    var setlogin = window.localStorage.getItem("user_login")
    console.log("setlogin", setlogin)
    if (!setlogin) {
      this.router.navigate(['login']);
      return false;
    }
    else {
      return true;

    }
  }

}
