import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuardServiceService } from './auth-guard-service.service';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'userlist',
    loadChildren: () =>
      import('./pages/userlist/userlist.module').then(m => m.UserlistModule),
    canActivate: [AuthGuardServiceService]
  },
  {
    path: 'userdetail', loadChildren: () => import('./pages/userdetail/userdetail.module').then(m => m.UserdetailModule),
    canActivate: [AuthGuardServiceService]
  },
  {
    path: 'edituser', loadChildren: () => import('./pages/edituser/edituser.module').then(m => m.EdituserModule),
    canActivate: [AuthGuardServiceService]
  },
  {
    path: 'createuser', loadChildren: () => import('./pages/createuser/createuser.module').then(m => m.CreateuserModule),
    canActivate: [AuthGuardServiceService]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
