import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../service/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
  displayedColumns: string[] = ['SRNO', 'name', 'email', 'description', 'Action'];
  dataSource = ELEMENT_DATA;
  constructor(public spinner: NgxSpinnerService, public router: Router, public loginService: LoginService) {
  }
  ngOnInit(): void {
    this.userlist();
  }
  userlist() {
    try {
      this.spinner.show();
      var token = window.localStorage.getItem("user_token")
      this.loginService.getuser(token).then((data: any) => {
        if (data[0].status == 201) {
          var list = JSON.parse(<string>data[0].json);
          if (list.flag == 1) {
            this.dataSource = list.data;
            this.spinner.hide();
          }
          else {
            this.spinner.hide();
            this.loginService.showerror(list.message)
          }
        }
        else if (data[0].status == 401) {
          window.localStorage.clear();
          this.spinner.hide();

          this.router.navigate(['login'])

        }
        else {
          var err = JSON.parse(<string>data[0].json);
          this.spinner.hide();
          this.loginService.showerror(err.message)
        }
      }, err => {
        this.spinner.hide();
        this.loginService.showerror('Encountered server error!');
      })
    }
    catch {
      this.spinner.hide();
      this.loginService.showerror('Encountered server error!');
    }
  }
  edit(element: UserList) {
    var postdata = {
      "name": element.name,
      "id": element.id,
      "profile_photo": element.profile_photo,
      "description": element.description
    }
    var data = btoa(JSON.stringify(postdata))
    this.router.navigate(['edituser'], { queryParams: { data: data } });
  }
  delete(element: UserList) {
    this.spinner.show()
    var token = window.localStorage.getItem("user_token")
    this.loginService.Delete_user(element.id, element.name, element.description, token).then((data: any) => {
      if (data[0].status == 201) {
        var list = JSON.parse(<string>data[0].json);
        this.userlist();
        this.loginService.showsuccess(list.message)
      }
      else if (data[0].status == 401) {
        window.localStorage.clear();
        this.spinner.hide();
        this.router.navigate(['login'])

      }
      else {
        this.spinner.hide()
        var err = JSON.parse(<string>data[0].json);
        if (err) {
          if (err.email) {
            this.loginService.showerror(err.email)
          }
        }
      }
    }, err => {
      this.spinner.hide()
      this.loginService.showerror('Encountered server error!');
    })

  }
  adduser() {
    this.router.navigate(['createuser']);
  }
  details(element: UserList) {
    this.router.navigate(['userdetail'], { queryParams: { data: element.id } });
  }
  logout() {
    try {
      this.spinner.show();
      var token = window.localStorage.getItem("user_token")
      this.loginService.logout(token).then((data: any) => {
        console.log("list", data)

        if (data[0].status == 200) {
          var list = JSON.parse(<string>data[0].json);
          this.spinner.hide();
          window.localStorage.clear();
          this.loginService.showsuccess(list.message)
          this.router.navigate(['login'])



        }
        else if (data[0].status == 401) {
          window.localStorage.clear();
          this.spinner.hide();

          this.router.navigate(['login'])

        }
        else {
          var err = JSON.parse(<string>data[0].json);
          this.spinner.hide();
          this.loginService.showerror(err.message)
        }
      }, err => {
        this.spinner.hide();
        this.loginService.showerror('Encountered server error!');
      })
    }
    catch {
      this.spinner.hide();
      this.loginService.showerror('Encountered server error!');
    }
  }
}
const ELEMENT_DATA: UserList[] = [];

export interface UserList {
  name: string;
  email: string;
  description: string;
  id: string;
  profile_photo: string;
  created_at: string;
  updated_at: string;

}