import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserlistRoutingModule } from './userlist-routing.module';
import { UserlistComponent } from './userlist.component';
import { MatTableModule } from '@angular/material/table';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    UserlistComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    UserlistRoutingModule,
    NgxSpinnerModule,
    MatIconModule
  ]
})
export class UserlistModule { }
