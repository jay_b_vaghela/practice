import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MustMatch } from '../../must-match.validator';
import { LoginService } from '../../service/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss']
})
export class EdituserComponent implements OnInit {

  email: any;
  password: any;
  LoginForm: any;
  submitted = false;
  confirmPassword: any;
  firstName: any;
  userimage: any;
  userimagedata: any;
  description: any;

  constructor(public route: ActivatedRoute, public spinner: NgxSpinnerService, public router: Router, public fb: FormBuilder, public loginService: LoginService) { }

  ngOnInit(): void {
    this.LoginForm = this.fb.group({
      firstName: ['', Validators.required],
      description: ['', Validators.required],
      userimage: [''],

    });
    var uri = this.route.snapshot.queryParams['data'] || null;
    var data = JSON.parse(atob(uri))
    this.email = data.email;
    this.description = data.description;
    this.firstName = data.name;
  }
  login() {
    try {
      this.submitted = true
      if (this.LoginForm.valid) {
        this.spinner.show()
        var token = window.localStorage.getItem("user_token")
        var uri = this.route.snapshot.queryParams['data'] || null;
        var data = JSON.parse(atob(uri))

        this.loginService.Edit_user(data.id, this.firstName, this.description, this.userimagedata, token).then((data: any) => {
          if (data[0].status == 201) {
            var list = JSON.parse(<string>data[0].json);
            this.spinner.hide()
            this.loginService.showsuccess(list.message)
            this.router.navigate(['/userlist']);
          }
          else if (data[0].status == 401) {
            window.localStorage.clear();
            this.spinner.hide();

            this.router.navigate(['login'])

          }
          else {
            this.spinner.hide()
            var err = JSON.parse(<string>data[0].json);
            if (err) {
              if (err.email) {
                this.loginService.showerror(err.email)
              }
            }
          }
        }, err => {
          this.spinner.hide()
          this.loginService.showerror('Encountered server error!');
        })

      }
      else {
        this.submitted = true

      }

    } catch {
      this.spinner.hide()
      this.loginService.showerror('Encountered server error!');
    }

  }
  get f() {
    return this.LoginForm.controls;
  }
  filechange(event: any) {
    this.userimagedata = event.target.files[0]
  }

}
