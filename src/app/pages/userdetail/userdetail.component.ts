import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../service/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.scss']
})
export class UserdetailComponent implements OnInit {
  userdata: UserList | undefined;
  constructor(public route: ActivatedRoute, public spinner: NgxSpinnerService, public router: Router, public loginService: LoginService) {
  }

  ngOnInit(): void {
    this.userget();
  }
  userget() {
    try {
      this.spinner.show();
      var token = window.localStorage.getItem("user_token")
      var id = this.route.snapshot.queryParams['data'] || null;
      this.loginService.getuserId(id, token).then((data: any) => {
        if (data[0].status == 201) {
          var list = JSON.parse(<string>data[0].json);

          this.userdata = list.data
          this.spinner.hide();


        }
        else if (data[0].status == 401) {
          window.localStorage.clear();
          this.spinner.hide();

          this.router.navigate(['login'])

        }
        else {
          var err = JSON.parse(<string>data[0].json);
          this.spinner.hide();
          this.loginService.showerror(err.message)
        }
      }, err => {
        this.spinner.hide();
        this.loginService.showerror('Encountered server error!');
      })
    }
    catch {
      this.spinner.hide();
      this.loginService.showerror('Encountered server error!');
    }
  }

}
export interface UserList {
  name: string;
  email: string;
  description: string;
  id: string;
  profile_photo: string;
  created_at: string;
  updated_at: string;

}