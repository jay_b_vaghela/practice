import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserdetailRoutingModule } from './userdetail-routing.module';
import { UserdetailComponent } from './userdetail.component';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    UserdetailComponent
  ],
  imports: [
    CommonModule,
    UserdetailRoutingModule,
    NgxSpinnerModule
  ]
})
export class UserdetailModule { }
