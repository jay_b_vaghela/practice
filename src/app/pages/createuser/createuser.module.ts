import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateuserRoutingModule } from './createuser-routing.module';
import { CreateuserComponent } from './createuser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    CreateuserComponent
  ],
  imports: [
    CommonModule,
    CreateuserRoutingModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule
  ]
})
export class CreateuserModule { }
