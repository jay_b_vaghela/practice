import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MustMatch } from '../../must-match.validator';
import { LoginService } from '../../service/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.scss']
})
export class CreateuserComponent implements OnInit {

  email: any;
  password: any;
  LoginForm: any;
  submitted = false;
  confirmPassword: any;
  firstName: any;
  userimage: any;
  userimagedata: any;
  description: any;

  constructor(public spinner: NgxSpinnerService, public router: Router, public fb: FormBuilder, public loginService: LoginService) { }

  ngOnInit(): void {
    this.LoginForm = this.fb.group({
      // email: ['', [Validators.required, Validators.email]],
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      description: ['', Validators.required],

      userimage: [''],

    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

  }
  login() {
    try {
      this.submitted = true
      if (this.LoginForm.valid) {
        this.spinner.show()
        var token = window.localStorage.getItem("user_token")

        this.loginService.Create_user(this.email, this.password, this.firstName, this.confirmPassword, this.description, this.userimagedata, token).then((data: any) => {
          if (data[0].status == 201) {
            var list = JSON.parse(<string>data[0].json);
            this.spinner.hide()
            this.loginService.showsuccess(list.message)
            this.router.navigate(['/userlist']);
          }
          else if (data[0].status == 401) {
            window.localStorage.clear();
            this.spinner.hide();

            this.router.navigate(['login'])

          }
          else {
            this.spinner.hide()
            var err = JSON.parse(<string>data[0].json);
            if (err) {
              if (err.email) {
                this.loginService.showerror(err.email)
              }
            }
          }
        }, err => {
          this.spinner.hide()
          this.loginService.showerror('Encountered server error!');
        })

      }
      else {
        this.submitted = true

      }

    } catch {
      this.spinner.hide()
      this.loginService.showerror('Encountered server error!');
    }

  }
  get f() {
    return this.LoginForm.controls;
  }
  filechange(event: any) {
    this.userimagedata = event.target.files[0]
  }
}
