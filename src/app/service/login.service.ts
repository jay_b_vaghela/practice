import { Injectable } from '@angular/core';
import { path } from '../enviroments/path';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  HttpModule,
} from '@angular/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(public http: Http, private toastr: ToastrService) { }
  login(email: any, password: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      formdata.append("email", email)
      formdata.append("password", password)
      this.http.post(path.BASE_URL + path.LOGIN, formdata).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }
  register(email: any, password: any, name: any, password_confirmation: any, description: any, profile_photo: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      formdata.append("email", email)
      formdata.append("password", password)
      formdata.append("name", name)
      formdata.append("password_confirmation", password_confirmation)
      formdata.append("description", description)
      formdata.append("profile_photo", profile_photo)


      this.http.post(path.BASE_URL + path.REGISTER, formdata).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }

  getuser(token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      let headers = new Headers();

      headers.append('Authorization', 'Bearer ' + token);
      let options = new RequestOptions({ headers: headers });
      this.http.post(path.BASE_URL + path.GET_USER, {}, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }
  Create_user(email: any, password: any, name: any, password_confirmation: any, description: any, profile_photo: any, token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      formdata.append("email", email)
      formdata.append("password", password)
      formdata.append("name", name)
      formdata.append("password_confirmation", password_confirmation)
      formdata.append("description", description)
      formdata.append("profile_photo", profile_photo)
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
      headers.append('Authorization', 'Bearer ' + token);
      this.http.post(path.BASE_URL + path.CREATE_USER, formdata, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }

  Edit_user(user_id: any, name: any, description: any, profile_photo: any, token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      formdata.append("name", name)
      formdata.append("description", description)
      formdata.append("profile_photo", profile_photo)
      formdata.append("user_id", user_id)

      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
      headers.append('Authorization', 'Bearer ' + token);
      this.http.post(path.BASE_URL + path.EDIT_USER, formdata, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }

  Delete_user(user_id: any, name: any, description: any, token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      formdata.append("name", name)
      formdata.append("description", description)
      formdata.append("user_id", user_id)
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
      headers.append('Authorization', 'Bearer ' + token);
      this.http.post(path.BASE_URL + path.DELETE_USER, formdata, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }

  getuserId(user_id: any, token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      let headers = new Headers();
      formdata.append("user_id", user_id)
      headers.append('Authorization', 'Bearer ' + token);
      let options = new RequestOptions({ headers: headers });
      this.http.post(path.BASE_URL + path.DETAILS_USER, formdata, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }
  logout(token: any) {
    return new Promise((resolve) => {
      let formdata = new FormData();
      let headers = new Headers();
      headers.append('Authorization', 'Bearer ' + token);
      let options = new RequestOptions({ headers: headers });
      this.http.post(path.BASE_URL + path.LOGOUT, {}, options).subscribe(
        (res: any) => {
          resolve([{ status: res.status, json: res["_body"] }]);
        },
        (err: any) => {
          resolve([{ status: err.status, json: err["_body"] }]);
        }
      );
    })


  }
  showsuccess(text: any) {
    this.toastr.success(text)
  }
  showerror(text: any) {
    this.toastr.error(text)
  }
}
