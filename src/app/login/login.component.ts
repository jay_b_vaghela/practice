import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { LoginService } from '../service/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: any;
  password: any;
  LoginForm: any;
  submitted = false;
  constructor(public spinner: NgxSpinnerService, public router: Router, public fb: FormBuilder, public loginService: LoginService) { }

  ngOnInit(): void {
    this.LoginForm = this.fb.group({
      // email: ['', [Validators.required, Validators.email]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  login() {
    try {
      if (this.LoginForm.valid) {
        this.spinner.show();
        this.loginService.login(this.email, this.password).then((data: any) => {
          if (data[0].status == 200) {
            var list = JSON.parse(<string>data[0].json);
            window.localStorage.setItem("user_token", list.access_token);
            window.localStorage.setItem("user_login", "true");

            this.loginService.showsuccess(list.message);
            this.router.navigate(['/userlist']);
            this.spinner.hide()
          }
          else {
            this.spinner.hide()
            var err = JSON.parse(<string>data[0].json);
            this.loginService.showerror(err.message);

          }
        }, err => {
          this.spinner.hide()
          this.loginService.showerror('Encountered server error!');
        })

      }
      else {
        this.submitted = true
      }
    } catch {
      this.spinner.hide()
      this.loginService.showerror('Encountered server error!');
    }


  }
  get f() {
    return this.LoginForm.controls;
  }
}
