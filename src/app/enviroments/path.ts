export const path = {
    BASE_URL: "http://dignizant.com/practical-task-api-doc/public/api",
    LOGIN: "/login",
    REGISTER: "/register",
    GET_USER: "/user-list",
    CREATE_USER: "/create-user",
    EDIT_USER: "/edit-user",
    DELETE_USER: "/delete-user",
    DETAILS_USER: "/user-detail",
    LOGOUT: "/logout"

}
